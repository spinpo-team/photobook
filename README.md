# Projeto PHOTOBOOK #

## Instruções ##

Baixe o repositório para seu computador ou dê um fork no projeto. 
Dentro do repositório contém o PSD do Layout, a imagem em PNG do Layout e os assets para o desenvolvimento.
Ao concluir o projeto, você deverá encaminhar para photobook@spinpo.com um .zip ou o link para um reposótorio git (com permissão de acesso público) com o seu projeto desenvolvido.

### Observações ###

Não há necessidade de usar o PSD. Todos os assets necessários se encontram dentro da pasta www/assets.

### Objetivo ###

Desenvolver o layout proposto utilizando somente HTML e CSS, sem auxilio de Frameworks e Libs

### Pontos importantes ###

Criar e manter uma estrutura organizacional do projeto e uma melhor aproximação do layout proposto.